package com.nuum.mspr2_inouicar_grc;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.nuum.mspr2_inouicar_grc.pojos.Vehicle;
import com.nuum.mspr2_inouicar_grc.pojos.VehicleAdapter;
import com.nuum.mspr2_inouicar_grc.pojos.VehicleList;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class CarDetailsActivity extends Activity
{

    TextView brand, model,licensePlate,price;
    ImageView picture;
    Vehicle vehicle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_details);

        brand = findViewById(R.id.brand);
        model = findViewById(R.id.model);
        licensePlate = findViewById(R.id.licensePlate);
        picture = findViewById(R.id.vehicle_picture);
        price = findViewById(R.id.price);

        //brand.setText("Brand :" +getIntent().getIntExtra());
        chargerVehicle(getIntent().getIntExtra("VehicleId", 0));

        Handler handler = new Handler();

        Runnable r = new Runnable() {

            @Override
            public void run() {
                miseAJourVue();
            }

        };
        handler.postDelayed(r, 1000);


        Button bCarList = findViewById(R.id.carList);

        bCarList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), CarListActivity.class);
                startActivity(i);

            }
        });
    }
    protected void miseAJourVue()
    {
        brand.setText("Brand :" + vehicle.getVehicle_brand());
        model.setText("Model : " + vehicle.getVehicle_model());
        licensePlate.setText("Plate n° : " + vehicle.getVehicle_licenceplate());
        price.setText("Daily price : " + vehicle.getVehicle_standarddailyrate() + "€");
       String imageName = "";
       imageName=vehicle.getVehicle_brand().toLowerCase();
       imageName += "_" + vehicle.getVehicle_model().toLowerCase();
       Uri imagePath = Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID +  "/drawable/"+imageName);

        picture.setImageURI(imagePath);
    }


    protected void chargerVehicle(int id)
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request myGetRequest = new Request.Builder()
                .url("http://192.168.1.18:8090/InouiCar/API/vehiculeList/"+id)
                //.url("http://nuumquizz.free.beeceptor.com/questions")
                .build();

        final CountDownLatch countDownLatch = new CountDownLatch(1);

        okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e)
            {
                System.out.println("Failure");
                e.printStackTrace();
                countDownLatch.countDown();

            }

            @Override
            public void onResponse(Response response) throws IOException {
                //le retour est effectué dans un thread différent
                final String text = response.body().string();
                final int statusCode = response.code();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //  Chargement des questions dans la base

                        Gson gson =new Gson();


                        System.out.println("ma question " +text.toString());
                        String [] splitBody = text.split("<body>");
                        String [] splitBody2 = splitBody[1].split("</body>");
                        String jsonString = splitBody2[0];
                        //System.out.println("IBNOIBJNOUIJBKN3" + text.split('pre-wrap;">')[1];

                        System.out.println("ma question " +jsonString.toString());

                        vehicle = gson.fromJson(jsonString, Vehicle.class);

                        System.out.println("haribo" + vehicle.toString());

                    }
                });

                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
