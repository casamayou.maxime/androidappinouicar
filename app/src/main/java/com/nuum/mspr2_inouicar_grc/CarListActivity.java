package com.nuum.mspr2_inouicar_grc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.nuum.mspr2_inouicar_grc.pojos.Vehicle;
import com.nuum.mspr2_inouicar_grc.pojos.VehicleAdapter;
import com.nuum.mspr2_inouicar_grc.pojos.VehicleList;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class CarListActivity extends Activity implements AdapterView.OnItemClickListener {

    ListView vListView;
    VehicleList myVehicleList;
    List<Vehicle> vehicles;

    String[] prenoms = new String[]{
            "Antoine", "Benoit", "Cyril", "David", "Eloise", "Florent",
            "Gerard", "Hugo", "Ingrid", "Jonathan", "Kevin", "Logan",
            "Mathieu", "Noemie", "Olivia", "Philippe", "Quentin", "Romain",
            "Sophie", "Tristan", "Ulric", "Vincent", "Willy", "Xavier", "Yann", "Zoé"
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_list);
        vListView = findViewById(R.id.listView);
        vListView.setOnItemClickListener(this);


        afficherListeVehicles();
        //afficherListeVehicles();
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        // Then you start a new Activity via Intent
        Intent intent = new Intent();
        intent.setClass(this, CarDetailsActivity.class);
        intent.putExtra("position", position);
        // Or / And
        intent.putExtra("id", id);
        intent.putExtra("VehicleId",vehicles.get(position).getVehicle_id());
        startActivity(intent);
    }


    private List<Vehicle> generateVehicles(){
        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        int color = 0xFFFF0000;
        vehicles.add(new Vehicle(color ,23,"Renault","Megane",
                "AL5768EH",1,
                "1",
                34.5,5));
        vehicles.add(new Vehicle(3,23,"Renault","Megane",
                "AL5768EH",1,
                "1",
                34.5,5));
        vehicles.add(new Vehicle(4,23,"Renault","Megane",
                "AL5768EH",1,
                "1",
                34.5,5));
        vehicles.add(new Vehicle(10,23,"Renault","Megane",
                "AL5768EH",1,
                "1",
                34.5,5));
        vehicles.add(new Vehicle(23,23,"Renault","Megane",
                "AL5768EH",1,
                "1",
                34.5,5));

        return vehicles;
    }

    private void afficherListeVehicles(){
        //List<Vehicle> vehicles = generateVehicles();

        System.out.println("SALUT");
        chargerVehicleList();

        Handler handler = new Handler();

        Runnable r = new Runnable() {

            @Override
            public void run() {
                miseAJourVue();
            }

        };
        handler.postDelayed(r, 1000);


        //VehicleAdapter adapter = new VehicleAdapter(CarListActivity.this, this.vehicles);
        //vListView.setAdapter(adapter);
    }

    protected void miseAJourVue()
    {
        //System.out.println("Bonjour" + myVehicleList.getVehicles().get(4).toString());
        VehicleAdapter adapter = new VehicleAdapter(CarListActivity.this, this.vehicles);
        vListView.setAdapter(adapter);

    }

    protected void chargerVehicleList() {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request myGetRequest = new Request.Builder()
                .url("http://192.168.1.18:8090/InouiCar/API/vehiculeList")
                //.url("http://nuumquizz.free.beeceptor.com/questions")
                .build();

        final CountDownLatch countDownLatch = new CountDownLatch(1);
        okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e)
            {
                System.out.println("Failure");
                e.printStackTrace();
                countDownLatch.countDown();

            }

            @Override
            public void onResponse(Response response) throws IOException {
                //le retour est effectué dans un thread différent
                final String text = response.body().string();
                final int statusCode = response.code();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //  Chargement des questions dans la base

                        Gson gson =new Gson();


                        System.out.println("ma question " +text.toString());
                        String [] splitBody = text.split("<body>");
                        String [] splitBody2 = splitBody[1].split("</body>");
                        String jsonString = splitBody2[0];
                        //System.out.println("IBNOIBJNOUIJBKN3" + text.split('pre-wrap;">')[1];

                        System.out.println("ma question " +jsonString.toString());

                        myVehicleList = gson.fromJson(jsonString, VehicleList.class);

                        System.out.println("ma question " +myVehicleList.toString());
                        System.out.println("Q1" + myVehicleList.getVehicles().get(0).toString());
                        //System.out.println("Q3" + myQuestionList.getQuestions().get(2).getLaQuestion().toString());

                        myVehicleList.getVehicles().remove(0);

                        vehicles =myVehicleList.getVehicles();

                        System.out.println("Vehicles"+vehicles.get(2).toString() );


                    }
                });
                countDownLatch.countDown();
            }
        });


        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }



}