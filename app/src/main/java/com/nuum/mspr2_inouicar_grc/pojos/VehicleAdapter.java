package com.nuum.mspr2_inouicar_grc.pojos;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nuum.mspr2_inouicar_grc.BuildConfig;
import com.nuum.mspr2_inouicar_grc.R;

import java.util.List;

public class VehicleAdapter extends ArrayAdapter<Vehicle> {

    public VehicleAdapter(Context context, List<Vehicle> vehicles) {
        super(context, 0, vehicles);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_vehicle,parent, false);

        }

        VehicleViewHolder viewHolder = (VehicleViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new VehicleViewHolder();
            viewHolder.vehicle_name = (TextView) convertView.findViewById(R.id.vehicle_name);
            viewHolder.vehicle_description = (TextView) convertView.findViewById(R.id.vehicle_description);
            viewHolder.picture = (ImageView) convertView.findViewById(R.id.picture);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Vehicle> vehicles
        Vehicle vehicle = getItem(position);
        viewHolder.vehicle_name.setText(vehicle.getVehicle_model());
        viewHolder.vehicle_description.setText(vehicle.getVehicle_brand());

     // Création de l'url pour afficher la vignette
     String imageName ="";
     imageName=vehicle.getVehicle_brand().toLowerCase();
     imageName += "_" + vehicle.getVehicle_model().toLowerCase();
     Uri imagePath = Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID +  "/drawable/"+imageName);
     viewHolder.picture.setImageURI(imagePath);

        return convertView;
    }

    private class VehicleViewHolder{
        public TextView vehicle_name;
        public TextView vehicle_description;
        public ImageView picture;

    }
}
