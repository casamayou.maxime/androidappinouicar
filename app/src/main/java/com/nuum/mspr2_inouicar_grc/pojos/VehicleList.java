package com.nuum.mspr2_inouicar_grc.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VehicleList {

    @SerializedName("vehicles")
    List<Vehicle> vehicles;

    public VehicleList()
    {

    }


    public VehicleList( List<Vehicle> myVehicles) {
        this.vehicles= new ArrayList<Vehicle>();
        for(int i=0;i<myVehicles.size();i++)
        {
            this.vehicles.add(myVehicles.get(i));
        }
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
