package com.nuum.mspr2_inouicar_grc.pojos;

import com.google.gson.annotations.SerializedName;

public class Vehicle {

    @SerializedName("vehicle_id")
    private int vehicle_id ;

    @SerializedName("fk_vehiclecategory_id")
    private int fk_vehiclecategory_id;

    @SerializedName("vehicle_brand")
    private String vehicle_brand;

    @SerializedName("vehicle_model")
    private String vehicle_model;

    @SerializedName("vehicle_licenceplate")
    private String vehicle_licenceplate;

    @SerializedName("vehicle_status")
    private int vehicle_status;

    @SerializedName("vehicle_insurancepolicy")
    private String vehicle_insurancepolicy;

    @SerializedName("vehicle_standarddailyrate")
    private Double vehicle_standarddailyrate;

    @SerializedName("vehicle_numberofseats")
    private int vehicle_numberofseats;

    public Vehicle(int vehicle_id, int fk_vehiclecategory_id, String vehicle_brand, String vehicle_model,
                   String vehicle_licenceplate, int vehicle_status, String vehicle_insurancepolicy,
                   Double vehicle_standarddailyrate, int vehicle_numberofseats) {
        super();
        this.vehicle_id = vehicle_id;
        this.fk_vehiclecategory_id = fk_vehiclecategory_id;
        this.vehicle_brand = vehicle_brand;
        this.vehicle_model = vehicle_model;
        this.vehicle_licenceplate = vehicle_licenceplate;
        this.vehicle_status = vehicle_status;
        this.vehicle_insurancepolicy = vehicle_insurancepolicy;
        this.vehicle_standarddailyrate = vehicle_standarddailyrate;
        this.vehicle_numberofseats = vehicle_numberofseats;
    }

    public int getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(int vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public int getFk_vehiclecategory_id() {
        return fk_vehiclecategory_id;
    }

    public void setFk_vehiclecategory_id(int fk_vehiclecategory_id) {
        this.fk_vehiclecategory_id = fk_vehiclecategory_id;
    }

    public String getVehicle_brand() {
        return vehicle_brand;
    }

    public void setVehicle_brand(String vehicle_brand) {
        this.vehicle_brand = vehicle_brand;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_licenceplate() {
        return vehicle_licenceplate;
    }

    public void setVehicle_licenceplate(String vehicle_licenceplate) {
        this.vehicle_licenceplate = vehicle_licenceplate;
    }

    public int getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(int vehicle_status) {
        this.vehicle_status = vehicle_status;
    }

    public String getVehicle_insurancepolicy() {
        return vehicle_insurancepolicy;
    }

    public void setVehicle_insurancepolicy(String vehicle_insurancepolicy) {
        this.vehicle_insurancepolicy = vehicle_insurancepolicy;
    }

    public Double getVehicle_standarddailyrate() {
        return vehicle_standarddailyrate;
    }

    public void setVehicle_standarddailyrate(Double vehicle_standarddailyrate) {
        this.vehicle_standarddailyrate = vehicle_standarddailyrate;
    }

    public int getVehicle_numberofseats() {
        return vehicle_numberofseats;
    }

    public void setVehicle_numberofseats(int vehicle_numberofseats) {
        this.vehicle_numberofseats = vehicle_numberofseats;
    }


    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicle_id=" + vehicle_id +
                ", fk_vehiclecategory_id=" + fk_vehiclecategory_id +
                ", vehicle_brand='" + vehicle_brand + '\'' +
                ", vehicle_model='" + vehicle_model + '\'' +
                ", vehicle_licenceplate='" + vehicle_licenceplate + '\'' +
                ", vehicle_status=" + vehicle_status +
                ", vehicle_insurancepolicy='" + vehicle_insurancepolicy + '\'' +
                ", vehicle_standarddailyrate=" + vehicle_standarddailyrate +
                ", vehicle_numberofseats=" + vehicle_numberofseats +
                '}';
    }
}
